# Hello World in 16 bytes
This was an idea from [this Computerphile video](https://youtu.be/5lFnKYCZT5o?t=83).


# Instructions

## HALT

Stops the processor.

Opcode: `0x00`


## NOP

Does nothing.

Opcode: `0x01`


## PRINC

Prints the byte at `mem[reg]` and increments `reg`.

Opcode: `0x02`


## PRINTNUM

Prints the byte at `mem[reg]` as a number.

Opcode: `0x03`


## INC

Increments `reg` by 1.

Opcode: `0x04`

Sets the carry bit if `reg` overflows. Clears otherwise.

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## DEC

Decrements `reg` by 1.

Opcode: `0x05`

Sets the carry bit if `reg` underflows. Clears otherwise.

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## SHL

Shifts `reg` by 1 bit to the left, effectively doubling it.

Opcode: `0x06`

Sets the carry bit if the most significant bit of `reg` is set. Clears otherwise.

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## SHR

Shifts `reg` by 1 bit to the right, effectively halving it.

Opcode: `0x07`

Sets the carry bit if the least significant bit of `reg` is set. Clears otherwise.

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## INPUT

Sets `reg` to the ASCII code of a character inputted by the user.

Opcode: `0x08`

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## INPUTNUM

Sets `reg` to a number inputted by the user.

Opcode: `0x09`

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## PRINTREG

Prints `reg`.

Opcode: `0x0a`


## PRINTREGNUM

Prints `reg` as a number.

Opcode: `0x0b`


## BEEPL

Sets `beepl` to `reg`.

Opcode: `0x0c`


## BEEP

Beeps with pitch `reg` and length `beepl`.

Opcode: `0x0d`



## LOAD

Loads the value from the operand into `reg`. This allows only for loading 4-bit values.

Opcode: `0x1x`

`x` - value to load

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## BNZ

Branches if `mem[reg]` is not equal to 0.

Opcode: `0x2x`

`x` - address to jump to


## READ

Loads the value from the memory into `reg`. This allows for loading 8-bit values.

Opcode: `0x3x`

`x` - address to load from

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## STORE

Stores the value of `reg` into memory.

Opcode: `0x4x`

`x` - address to store to


## ADD

Adds the value from memory into `reg`.

Opcode: `0x5x`

`x` - address of the value to add

Sets the carry bit if `reg` overflows. Clears otherwise.

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## JMP

Always branches.

Opcode: `0x6x`

`x` - address to jump to


## BCS

Branches if the carry bit is set.

Opcode: `0x7x`

`x` - address to jump to


## BCC

Branches if the carry bit is not set (is clear).

Opcode: `0x8x`

`x` - address to jump to


## SUB

Subtracts the value from memory into `reg`.

Opcode: `0x9x`

`x` - address of the value to add

Sets the carry bit if `reg` underflows. Clears otherwise.

Sets the equal bit if `reg` is equal to 0. Clears otherwise.


## BEQ

Branches if the equal bit is set.

Opcode: `0xax`

`x` - address to jump to


## BNE

Branches if the equal bit is not set (is clear).

Opcode: `0xbx`

`x` - address to jump to


## CMP

Subtracts the value from memory from `reg`. Doesn't change `reg`.

Opcode: `0xcx`

`x` - address of the value to add

Sets the carry bit if the result underflows. Clears otherwise.

Sets the equal bit if the result is equal to 0. Clears otherwise.

Alternatively, you can think of it like this:

Compares `reg` with memory. Sets the carry bit if memory is greater than `reg`. Sets the equal bit if memory is equal to `reg`.
