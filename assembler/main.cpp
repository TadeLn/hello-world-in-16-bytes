#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <algorithm>



void strip_in_place(std::string& str) {
    size_t first = str.find_first_not_of(' ');
    size_t last = str.find_last_not_of(' ');
    str = str.substr(first, (last-first+1));
}

void to_upper(std::string& str) {
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

enum Opcode {
    UNDEFINED   = -1,
    HALT        = 0x0,  // HALT the processor
    NOP         = 0x1,  // NO Operation
    PRINC       = 0x2,  // PRInt value at register and INCrement register
    PRINTNUM    = 0x3,  // PRINT value at register as a NUMber
    INC         = 0x4,  // INCrement register
    DEC         = 0x5,  // DECrement register
    SHL         = 0x6,  // SHift Left register
    SHR         = 0x7,  // SHift Right register
    INPUT       = 0x8,  // Get INPUT
    INPUTNUM    = 0x9,  // Get NUMeric INPUT
    PRINTREG    = 0xa,  // PRINT REGister
    PRINTREGNUM = 0xb,  // PRINT REGister as a NUMber
    BEEPL       = 0xc,  // Set BEEP Length to register
    BEEP        = 0xd,  // BEEP with frequency from register
    LOAD        = 0x10, // LOAD value into register
    BNZ         = 0x20, // Branch if memory is Not Zero
    READ        = 0x30, // READ memory into register
    STORE       = 0x40, // STORE register in memory
    ADD         = 0x50, // ADD value from memory to register
    JMP         = 0x60, // JuMP to address
    BCS         = 0x70, // Branch on Carry Set
    BCC         = 0x80, // Branch on Carry Clear
    SUB         = 0x90, // SUBtracts value in memory from register
    BEQ         = 0xa0, // Branches if EQual
    BNE         = 0xb0, // Branches if Not Equal
    CMP         = 0xc0, // CoMPare register to memory
    DB          = -2,   // Define Byte
    DC          = -3    // Define Character
};

enum ArgType {
    NONE,
    NUM,
    CHAR
};

struct OpcodeData {
    Opcode type = UNDEFINED;
    ArgType arg = NONE;
};

const std::map<std::string, OpcodeData> opcodes{
    { "HALT",        { HALT,        NONE } },
    { "NOP",         { NOP,         NONE } },
    { "PRINC",       { PRINC,       NONE } },
    { "PRINTNUM",    { PRINTNUM,    NONE } },
    { "INC",         { INC,         NONE } },
    { "DEC",         { DEC,         NONE } },
    { "SHL",         { SHL,         NONE } },
    { "SHR",         { SHR,         NONE } },
    { "INPUT",       { INPUT,       NONE } },
    { "INPUTNUM",    { INPUTNUM,    NONE } },
    { "PRINTREG",    { PRINTREG,    NONE } },
    { "PRINTREGNUM", { PRINTREGNUM, NONE } },
    { "BEEPL",       { BEEPL,       NONE } },
    { "BEEP",        { BEEP,        NONE } },
    { "LOAD",        { LOAD,        NUM  } },
    { "BNZ",         { BNZ,         NUM  } },
    { "READ",        { READ,        NUM  } },
    { "STORE",       { STORE,       NUM  } },
    { "ADD",         { ADD,         NUM  } },
    { "JMP",         { JMP,         NUM  } },
    { "BCS",         { BCS,         NUM  } },
    { "BCC",         { BCC,         NUM  } },
    { "SUB",         { SUB,         NUM  } },
    { "BEQ",         { BEQ,         NUM  } },
    { "BNE",         { BNE,         NUM  } },
    { "CMP",         { CMP,         NUM  } },
    { "DB",          { DB,          NUM  } },
    { "DC",          { DC,          CHAR } }
};

int main(int argc, char** argv) {
    if (argc == 3) {
        bool error = false;
        try {
            std::ifstream input;
            input.open(argv[1]);
            if (!input.is_open()) throw std::runtime_error("input file couldn't be opened");

            std::ofstream output;
            output.open(argv[2]);
            if (!output.is_open()) throw std::runtime_error("output file couldn't be opened");

            std::stringstream result;

            std::string line;
            int lineNo = 0;
            while (std::getline(input, line)) {
                lineNo++;
                if (line.empty()) continue;
                strip_in_place(line);

                if (line[0] == '#') continue;
                if (line.empty()) continue;


                std::string token;
                OpcodeData opcode = { UNDEFINED };
                char arg = '\0';

                try {
                    while (!line.empty()) {
                        auto index = line.find_first_of(' ');
                        token = line.substr(0, index);
                        if (index > line.size()) {
                            line = "";
                        } else {
                            line = line.substr(index + 1);
                        }

                        if (token.empty()) continue;

                        if (opcode.type == UNDEFINED) {
                            to_upper(token);

                            auto it = opcodes.find(token);
                            if (it == opcodes.end()) {
                                break;
                            }

                            opcode = it->second;
                            if (opcode.arg == NONE) {
                                // No argument, stop reading this line
                                break;
                            }

                        } else if (opcode.arg == CHAR) {
                            if (token.size() < 0) {
                                throw std::runtime_error("DC argument empty");
                            }
                            arg = token[0];
                            break;

                        } else if (opcode.arg == NUM) {
                            arg = std::stoi(token, nullptr, 0);
                            break;
                        }
                    }

                    if (opcode.type < 0) {
                        if (opcode.type == DB || opcode.type == DC) {
                            result << char(arg);
                        } else {
                            throw std::runtime_error("invalid opcode");
                        }
                    } else {
                        if (opcode.arg == NONE) {
                            result << char(opcode.type);

                        } else if (opcode.arg == NUM) {
                            arg &= 0xf;
                            result << char(opcode.type | arg);

                        } else if (opcode.arg == CHAR) {
                            arg &= 0xf;
                            result << char(opcode.type | arg);
                        }
                    }

                } catch (std::exception& e) {
                    std::cerr << "error: line " << lineNo << ": " << e.what() << "\n";
                    error = true;
                }
            }

            input.close();

            if (error) {
                output.close();
                throw std::runtime_error("compilation aborted");
            }

            output << result.str();
            output.close();
        } catch (std::exception& e) {
            std::cerr << "error: " << e.what() << "\n";
            return 2;
        }

    } else {
        std::cerr << "usage: " << argv[0] << " [input file] [output file]\n";
        return 1;
    }
}
