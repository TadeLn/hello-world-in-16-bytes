#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>



enum OpcodeData {
    SPECIAL = 0x00,
    LOAD    = 0x10,
    BNZ     = 0x20,
    READ    = 0x30,
    STORE   = 0x40,
    ADD     = 0x50,
    JMP     = 0x60,
    BCS     = 0x70,
    BCC     = 0x80,
    SUB     = 0x90,
    BEQ     = 0xa0,
    BNE     = 0xb0,
    CMP     = 0xc0
};

enum SpecialOpcode {
    HALT        = 0x0,
    NOP         = 0x1,
    PRINC       = 0x2,
    PRINTNUM    = 0x3,
    INC         = 0x4,
    DEC         = 0x5,
    SHL         = 0x6,
    SHR         = 0x7,
    INPUT       = 0x8,
    INPUTNUM    = 0x9,
    PRINTREG    = 0xa,
    PRINTREGNUM = 0xb,
    BEEPL       = 0xc,
    BEEP        = 0xd
};

#ifdef NO_BEEP

void beep(unsigned char pitch, unsigned char length) {
    std::cout << std::dec << "<beep: "
        "pitch="  << (int)pitch  << " "
        "length=" << (int)length << ">\n";
}

#else

#include <unistd.h>
#include <sys/wait.h>
auto frequencies = std::array<int, 128>();

void beep(unsigned char pitch, unsigned char length) {
    int freq = frequencies[pitch % 128];
    
    /*
    std::cout << std::dec << "<beep: "
        "pitch="  << (int)pitch  << " "
        "freq="   << (int)freq   << " "
        "length=" << (int)length << ">\n";
    */

    int wstatus;
    if (fork() == 0) {
        execlp("beep", "beep", "-f", std::to_string(freq).c_str(), "-l", std::to_string(int((double(length) / 255.0) * 1000.0)).c_str(), NULL);
    } else {
        wait(&wstatus);
    }
}

#endif

int main(int argc, char** argv) {
    #ifndef NO_BEEP
    double root = pow(2, 1.0 / 12.0);
    for (int i = 0; i < frequencies.size(); i++) {
        frequencies[i] = 440.0 * pow(root, (i - 69));
    }
    #endif


    if (argc == 2) {
        std::ifstream f;
        f.open(argv[1]);
        if (!f.is_open()) {
            std::cerr << "error: file couldn't be opened\n";
            return 1;
        }

        char mem[16];
        unsigned char reg = 0;
        unsigned char pc = 0;
        unsigned char beepl = 0;
        bool carry = false;
        bool equal = false;

        f.read(mem, 16);

        while (true) {
            unsigned char instruction = mem[pc];
            unsigned char opcode = instruction & 0xf0;
            unsigned char arg = instruction & 0xf;

            #ifdef VERBOSE
                std::cout << std::hex << "\t| "
                    "PC: "  << std::setw(2) << std::setfill('0') << (int)pc << " "
                    "REG: " << std::setw(2) << std::setfill('0') << (int)reg << " "
                    "BEEPL: " << std::setw(2) << std::setfill('0') << (int)beepl << " "
                    "C: "   << (int)carry << " "
                    "EQ: "  << (int)equal << " "
                    "MEM: ";
                for (int i = 0; i < 16; i++) {
                    std::cout << std::setw(2) << std::setfill('0') << int((unsigned char)(mem[i])) << " ";
                }
                std::cout << "| " << std::setw(15) << std::setfill(' ');
            #endif

            switch (opcode) {

                case SPECIAL:
                    {
                        switch (arg) {
                            
                            // 0x0
                            case HALT:
                                #ifdef VERBOSE
                                    std::cout << "HALT" << std::setw(0) << "\n";
                                #endif
                                goto halt;

                            // 0x1
                            case NOP:
                                #ifdef VERBOSE
                                    std::cout << "NOP" << std::setw(0) << "\n";
                                #endif
                                break;

                            // 0x2
                            case PRINC:
                                #ifdef VERBOSE
                                    std::cout << "PRINC" << std::setw(0) << "\n";
                                #endif
                                std::cout << mem[reg++];
                                break;

                            // 0x3
                            case PRINTNUM:
                                #ifdef VERBOSE
                                    std::cout << "PRINTNUM" << std::setw(0) << "\n";
                                #endif
                                std::cout << std::setw(0) << std::dec << int((unsigned char)(mem[reg]));
                                break;

                            // 0x4
                            case INC:
                                #ifdef VERBOSE
                                    std::cout << "INC" << std::setw(0) << "\n";
                                #endif
                                reg++;
                                carry = reg == 0x00;
                                equal = reg == 0x00;
                                break;

                            // 0x5
                            case DEC:
                                #ifdef VERBOSE
                                    std::cout << "DEC" << std::setw(0) << "\n";
                                #endif
                                reg--;
                                carry = reg == 0xff;
                                equal = reg == 0x00;
                                break;

                            // 0x6
                            case SHL:
                                #ifdef VERBOSE
                                    std::cout << "SHL" << std::setw(0) << "\n";
                                #endif
                                carry = (reg & 0b10000000);
                                reg <<= 1;
                                equal = reg == 0x00;
                                break;

                            // 0x7
                            case SHR:
                                #ifdef VERBOSE
                                    std::cout << "SHR" << std::setw(0) << "\n";
                                #endif
                                carry = (reg & 0b00000001);
                                reg >>= 1;
                                equal = reg == 0x00;
                                break;

                            // 0x8
                            case INPUT:
                                #ifdef VERBOSE
                                    std::cout << "INPUT" << std::setw(0) << "\n";
                                #endif
                                std::cout << "\n$ ";
                                {
                                    char c;
                                    std::cin.get(c);
                                    if (std::cin.eof()) {
                                        goto halt;
                                    } else {
                                        reg = c;
                                    }
                                }
                                equal = reg == 0x00;
                                break;

                            // 0x9
                            case INPUTNUM:
                                #ifdef VERBOSE
                                    std::cout << "INPUTNUM" << std::setw(0) << "\n";
                                #endif
                                {
                                    int num;
                                    bool valid = false;
                                    while (!valid) {
                                        std::string line;
                                        std::cout << "\n> ";
                                        std::getline(std::cin, line);
                                        try {
                                            num = std::stoi(line);
                                            valid = true;
                                        } catch (std::exception& e) {
                                            if (std::cin.eof()) {
                                                goto halt;
                                            }
                                            std::cerr << "<invalid number, try again>\n";
                                            valid = false;
                                        }
                                    }
                                    reg = num;
                                }
                                equal = reg == 0x00;
                                break;

                            // 0xa
                            case PRINTREG:
                                #ifdef VERBOSE
                                    std::cout << "PRINC" << std::setw(0) << "\n";
                                #endif
                                std::cout << reg;
                                break;

                            // 0xb
                            case PRINTREGNUM:
                                #ifdef VERBOSE
                                    std::cout << "PRINTNUM" << std::setw(0) << "\n";
                                #endif
                                std::cout << std::setw(0) << std::dec << int(reg);
                                break;

                            // 0xc
                            case BEEPL:
                                #ifdef VERBOSE
                                    std::cout << "BEEPL" << std::setw(0) << "\n";
                                #endif
                                beepl = reg;
                                break;

                            // 0xd
                            case BEEP:
                                #ifdef VERBOSE
                                    std::cout << "BEEP" << std::setw(0) << "\n";
                                #endif
                                beep(reg, beepl);
                                break;

                            default:
                                // Unknown special opcode, act as NOP
                                #ifdef VERBOSE
                                    std::cout << "UNKNOWN SPECIAL" << std::setw(0) << " 0x" << (int)arg << "\n";
                                #endif
                                break;
                        }
                    }
                    break;

                // 0x10
                case LOAD:
                    #ifdef VERBOSE
                        std::cout << "LOAD" << std::setw(0) << " 0x" << (int)arg << "\n";
                    #endif
                    reg = arg;
                    equal = reg == 0x00;
                    break;

                // 0x20
                case BNZ:
                    #ifdef VERBOSE
                        std::cout << "BNZ" << std::setw(0) << " 0x" << (int)arg;
                    #endif
                    if (mem[reg % 16] != 0) {
                        #ifdef VERBOSE
                            std::cout << " (branch taken)\n";
                        #endif
                        pc = arg - 1;
                        pc &= 0xf;
                    } else {
                        #ifdef VERBOSE
                            std::cout << " (branch not taken)\n";
                        #endif
                    }
                    break;

                // 0x30
                case READ:
                    #ifdef VERBOSE
                        std::cout << "READ" << std::setw(0) << " 0x" << (int)arg << "\n";
                    #endif
                    reg = mem[arg];
                    equal = reg == 0x00;
                    break;

                // 0x40
                case STORE:
                    #ifdef VERBOSE
                        std::cout << "STORE" << std::setw(0) << " 0x" << (int)arg << "\n";
                    #endif
                    mem[arg] = reg;
                    break;

                // 0x50
                case ADD:
                    #ifdef VERBOSE
                        std::cout << "ADD" << std::setw(0) << " 0x" << (int)arg << "\n";
                    #endif
                    {
                        unsigned char oldreg = reg;
                        reg += int((unsigned char)mem[arg]);
                        carry = reg < oldreg;
                        equal = reg == 0x00;
                    }
                    break;

                // 0x60
                case JMP:
                    #ifdef VERBOSE
                        std::cout << "JMP" << std::setw(0) << " 0x" << (int)arg << "\n";
                    #endif
                    pc = arg - 1;
                    pc &= 0xf;
                    break;

                // 0x70
                case BCS:
                    #ifdef VERBOSE
                        std::cout << "BCS" << std::setw(0) << " 0x" << (int)arg;
                    #endif
                    if (carry) {
                        #ifdef VERBOSE
                            std::cout << " (branch taken)\n";
                        #endif
                        pc = arg - 1;
                        pc &= 0xf;
                    } else {
                        #ifdef VERBOSE
                            std::cout << " (branch not taken)\n";
                        #endif
                    }
                    break;

                // 0x80
                case BCC:
                    #ifdef VERBOSE
                        std::cout << "BCC" << std::setw(0) << " 0x" << (int)arg;
                    #endif
                    if (!carry) {
                        #ifdef VERBOSE
                            std::cout << " (branch taken)\n";
                        #endif
                        pc = arg - 1;
                        pc &= 0xf;
                    } else {
                        #ifdef VERBOSE
                            std::cout << " (branch not taken)\n";
                        #endif
                    }
                    break;

                // 0x90
                case SUB:
                    #ifdef VERBOSE
                        std::cout << "SUB" << std::setw(0) << " 0x" << (int)arg << "\n";
                    #endif
                    {
                        unsigned char oldreg = reg;
                        reg -= int((unsigned char)mem[arg]);
                        carry = reg > oldreg;
                    }
                    equal = reg == 0x00;
                    break;

                // 0xa0
                case BEQ:
                    #ifdef VERBOSE
                        std::cout << "BEQ" << std::setw(0) << " 0x" << (int)arg;
                    #endif
                    if (equal) {
                        #ifdef VERBOSE
                            std::cout << " (branch taken)\n";
                        #endif
                        pc = arg - 1;
                        pc &= 0xf;
                    } else {
                        #ifdef VERBOSE
                            std::cout << " (branch not taken)\n";
                        #endif
                    }
                    break;

                // 0xb0
                case BNE:
                    #ifdef VERBOSE
                        std::cout << "BNE" << std::setw(0) << " 0x" << (int)arg;
                    #endif
                    if (!equal) {
                        #ifdef VERBOSE
                            std::cout << " (branch taken)\n";
                        #endif
                        pc = arg - 1;
                        pc &= 0xf;
                    } else {
                        #ifdef VERBOSE
                            std::cout << " (branch not taken)\n";
                        #endif
                    }
                    break;

                // 0xc0
                case CMP:
                    #ifdef VERBOSE
                        std::cout << "CMP" << std::setw(0) << " 0x" << (int)arg << "\n";
                    #endif
                    carry = ((unsigned char)mem[arg]) > reg;
                    equal = reg == ((unsigned char)mem[arg]);
                    break;

                default:
                    // Unknown opcode, act as NOP
                    #ifdef VERBOSE
                        std::cout << "UNKNOWN\n";
                    #endif
                    break;
            }

            pc++;
            pc &= 0xf;
        }

        halt:
        return 0;
    } else {
        std::cerr << "usage: " << argv[0] << " [input file]\n";
        return 1;
    }
}
